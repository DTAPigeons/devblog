﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ComponentModel.DataAnnotations;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;

namespace DevBlog.Models {
    public class CommentModel : BaseModel {
        [ForeignKey("Author")]
        public int AuthorID { get; set; }
        [ForeignKey("Post")]
        public int PostID { get; set; }
        [MaxLength]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public DateTime TimePosted { get; set; }
        [ForeignKey("ResponseTo")]
        public int? ResponseToID { get; set; }

        public virtual UserModel Author { get; set; }
        public virtual List<CommentModel> Responses { get; set; }
        public virtual CommentModel ResponseTo { get; set; }
        public virtual PostModel Post { get; set; }
    }
}