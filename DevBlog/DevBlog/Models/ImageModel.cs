﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DevBlog.Models {
    public class ImageModel:BaseModel {
        [ForeignKey("Post")]
        public int PostID { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Image")]
        [Required(ErrorMessage = "Please choose image to upload.")]
        public string Path { get; set; }

        public virtual PostModel Post { get; set; }
    }
}