﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace DevBlog.Models {
    public class UserModel:BaseModel {
        [StringLength(100)]
        public string UserName { get; set; }
        public string Role { get; set; }

        public virtual List<PostModel> Posts { get; set; }
        public virtual List<CommentModel> Comments { get; set; }
        //[Required]
        //public virtual ImageModel ProfilePicture { get; set; }
    }
}