﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace DevBlog.Models {
    public class PostModel: BaseModel {
        [ForeignKey("Author")]
        public int AuthorID { get; set; }
        public DateTime TimePosted { get; set; }
        [StringLength(256)]
        public string Title { get; set; }
        [MaxLength]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }

        public virtual UserModel Author { get; set; }
        public virtual List<ImageModel> Images { get; set; }
        public virtual List<CommentModel> Comments { get; set; }
    }
}