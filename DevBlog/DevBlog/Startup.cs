﻿using DevBlog.DataAccess;
using DevBlog.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DevBlog.Startup))]
namespace DevBlog
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            createRolesandUsers();
        }

        private void createRolesandUsers() {
            ApplicationDbContext context = new ApplicationDbContext();

            var roleManager = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));
            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));


            // In Startup iam creating first Dev Role and creating a default Admin User    
            if (!roleManager.RoleExists("Dev")) {

                UserRepository rep = new UserRepository();

                // first we create Dev rool   
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Dev";
                roleManager.Create(role);

                //Here we create a Admin super user who will maintain the website                  

                var user = new ApplicationUser();
                user.UserName = "DeathToAllPigeons";
                user.Email = "pev97@abv.bg";

                string userPWD = "fuckyou97";

                var chkUser = UserManager.Create(user, userPWD);

                if (!rep.UserNameExists("DeathToAllPigeons")) {
                    rep.Save(new UserModel() { UserName = "DeathToAllPigeons" , Role = "fuckyou97" });
                }

                //Add default User to Role Admin   
                if (chkUser.Succeeded) {
                    var result1 = UserManager.AddToRole(user.Id, "Dev");

                }
            }

            // creating Creating Reader role    
            if (!roleManager.RoleExists("Reader")) {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Reader";
                roleManager.Create(role);

                var user = new ApplicationUser();
                user.UserName = "Chungus";
                user.Email = "pavkat97@abv.bg";

                string userPWD = "123456";

                var chkUser = UserManager.Create(user, userPWD);
   
                if (chkUser.Succeeded) {
                    var result1 = UserManager.AddToRole(user.Id, "Reader");

                }
            }

            // creating Creating Banned role 
            if (!roleManager.RoleExists("Banned")) {
                var role = new Microsoft.AspNet.Identity.EntityFramework.IdentityRole();
                role.Name = "Banned";
                roleManager.Create(role);

            }
        }
    }
}
