﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.ViewModels.Shared;
using DevBlog.Models;

namespace DevBlog.Helpers {
    public class ControlerHelper {
        public static ListingViewModel<T> SetUpLisingViewModel<T>(ListingViewModel<T> model, int entityCount) where T:DisplayViewModel{
            if (model.Pager == null) { model.Pager = new Pager(); }

            if (model.Pager.Page <= 0) { model.Pager.Page = 1; }

            if (model.Pager.PageSize <= 0) { model.Pager.PageSize = Pager.MAX_PAGE_SIZE; }

            model.Pager.PagesCount = (int)Math.Ceiling(entityCount / (double)model.Pager.PageSize);

            model.Items = new List<T>();

            return model;
        }
    }
}