﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevBlog.DataAccess;
using DevBlog.ViewModels.PostViewModels;
using DevBlog.ViewModels.Shared;
using DevBlog.Models;
using System.Web.Security;
using DevBlog.Helpers;

namespace DevBlog.Controllers
{
    [Authorize(Roles = "Dev")]
    public class PostController : Controller
    {
        private PostRepository repository = new PostRepository();

        // GET: Post
        [AllowAnonymous]
        public ActionResult Index(PostListingViewModel model, int page=1,bool descending =true, string sortParameter = "Date")
        {
            if (page > 1) { model = ControlerHelper.SetUpLisingViewModel<PostListedViewModel>(new PostListingViewModel(page), repository.GetCount()) as PostListingViewModel; }
            else { model = ControlerHelper.SetUpLisingViewModel<PostListedViewModel>(model, repository.GetCount()) as PostListingViewModel; }

            foreach(PostModel post in repository.GetAll(model.Pager.Page, model.Pager.PageSize,descending,sortParameter)) {
                PostListedViewModel item = new PostListedViewModel();
                item.SetDataFromModel(post);
                model.Items.Add(item);
            }

            return View(model);
        }

        [AllowAnonymous]
        public ActionResult Details(PostDisplayViewModel model) {

            PostModel post = repository.GetByID(model.ID);

            if (post == null) {
                return HttpNotFound();
            }

            model.SetDataFromModel(post);

            return View(model);
        }

        public ActionResult Create() {
            return View();
        }

        [HttpPost]
        public ActionResult Create(PostCreatViewModel model) {
            if (ModelState.IsValid) {
                PostModel post = model.CreateModel();

                UserRepository rep = new UserRepository();

                post.AuthorID = rep.GetIDByUserName(User.Identity.Name);

                repository.Save(post);

                return RedirectToAction("Index");
            }
            else {
                return View();
            }
        }

        public ActionResult Edit(int id) {
            PostModel post = repository.GetByID(id);
            if (post == null) {
                return HttpNotFound();
            }

            PostCreatViewModel model = new PostCreatViewModel() { };
            model.SetDataFromModel(post);

            return View(model);
        }


        [HttpPost]
        public ActionResult Edit(PostCreatViewModel model) {
            if (!ModelState.IsValid) {
                return View(model);
            }

            PostModel post = repository.GetByID(model.ID);

            if (post == null) {
                return HttpNotFound();
            }
            post.Title = model.Title;
            post.Content = model.Content;
            repository.Save(post);

            return RedirectToAction("Details", new { ID = post.ID });
        }

        public ActionResult Delete(int id) {
            PostModel post = repository.GetByID(id);
            if (post == null) {
                return HttpNotFound();
            }

            repository.Delete(post);

            return RedirectToAction("Index");
        }
    }
}