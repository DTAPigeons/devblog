﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevBlog.ViewModels.CommentViewModels;
using DevBlog.ViewModels.PostViewModels;
using DevBlog.DataAccess;
using DevBlog.Models;

namespace DevBlog.Controllers
{
    [Authorize(Roles ="Dev,Reader")]
    public class CommentController : Controller
    {
        CommentRepository repository = new CommentRepository();

        public ActionResult Index(string userName) {
            if(userName!=User.Identity.Name && !User.IsInRole("Dev")) {
                return HttpNotFound();
            }
            UserRepository rep = new UserRepository();
            UserModel user = rep.GetByID(rep.GetIDByUserName(userName));
            if (user != null) {
                List<CommentDisplayViewModel> model = new List<CommentDisplayViewModel>();
                foreach(CommentModel comment in user.Comments) {
                    CommentDisplayViewModel display = new CommentDisplayViewModel();
                    display.SetDataFromModel(comment);
                    model.Add(display);
                }
                return View("UsersComments", model);
            }
            else {
                return HttpNotFound();
            }
        }

        [HttpPost]
        public ActionResult Create(CommentCreateViewModel model) {
            if (ModelState.IsValid) {
                UserRepository rep = new UserRepository();

                CommentModel comment = model.CreatModel();

                comment.AuthorID = rep.GetIDByUserName(User.Identity.Name);

                repository.Save(comment);

                return RedirectToAction("Details", "Post", new { ID = model.PostID });
            }
            else {
                    return RedirectToAction("Details", "Post", new { ID = model.PostID });
            }
        }


        public ActionResult Edit(int id) {
            CommentModel comment = repository.GetByID(id);
            if (comment == null || comment.Author.UserName!=User.Identity.Name) {
                return HttpNotFound();
            }

            CommentCreateViewModel model = new CommentCreateViewModel();
            model.SetDataFromModel(comment);

            return View(model);
        }

        [HttpPost]
        public ActionResult Edit(CommentCreateViewModel model) {

            if (!ModelState.IsValid) {
                return View(model);
            }

            CommentModel comment = repository.GetByID(model.ID);

            if (comment == null || comment.Author.UserName != User.Identity.Name) {
                return HttpNotFound();
            }
            comment.Content = model.Content;
            repository.Save(comment);

            return RedirectToAction("Details","Post", new { ID = comment.PostID });
        }

        public ActionResult Delete(int id) {
            CommentModel comment = repository.GetByID(id);
            if (comment == null || (comment.Author.UserName != User.Identity.Name && !User.IsInRole("Dev"))) {
                return HttpNotFound();
            }
            int postID = comment.PostID;
            repository.Delete(comment);

            return RedirectToAction("Details", "Post", new { ID = postID});
        }

        public ActionResult Respond(int responseToID) {
            CommentModel comment = repository.GetByID(responseToID);

            if (comment == null) {
                return HttpNotFound();
            }

            CommentDisplayViewModel baseComment = new CommentDisplayViewModel();
            baseComment.SetDataFromModel(comment);

            CommentRespondViewModel model = new CommentRespondViewModel() { ResposeTo = baseComment , PostID = comment.PostID};

            if (comment.ResponseToID > 0 && comment.ResponseToID!=null) {
                model.ResponseToID = comment.ResponseToID;
            }
            else {
                model.ResponseToID = comment.ID;
            }

            return View(model); 
        }

        [HttpPost]
        public ActionResult Respond(CommentRespondViewModel response) {
            if (ModelState.IsValid) {
                UserRepository rep = new UserRepository();

                CommentModel comment = response.CreatModel();

                comment.AuthorID = rep.GetIDByUserName(User.Identity.Name);

                repository.Save(comment);

                return RedirectToAction("Details", "Post", new { ID = response.PostID });
            }
            else { return View(response); }
        }
    }
}