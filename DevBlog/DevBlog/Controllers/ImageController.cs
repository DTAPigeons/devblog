﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevBlog.ViewModels.ImageViewModels;
using System.Globalization;
using DevBlog.DataAccess;
using DevBlog.Models;

namespace DevBlog.Controllers
{
    [Authorize(Roles = "Dev")]
    public class ImageController : Controller
    {
        ImageRepository repository = new ImageRepository();

        [HttpPost]
        public ActionResult Upload(ImageUploadViewModel model)
        {
            if (ModelState.IsValid) {
                string image = DateTime.UtcNow.ToString("yyMMddhhmmssffff", CultureInfo.InvariantCulture)
                    + System.IO.Path.GetFileName(model.ImageFile.FileName);
                model.Path = "~/Images/" + image;
                string path = System.IO.Path.Combine(Server.MapPath("~/Images"),image);
                model.ImageFile.SaveAs(path);

                repository.Save(model.CreatModel());

                return RedirectToAction("Details","Post",new { ID = model.PostID});
            }
            else { return RedirectToAction("Details", "Post", new { ID = model.PostID }); }
        }

        public ActionResult Delete(int id) {
            ImageModel image = repository.GetByID(id);
            if (image != null) {
                int postID = (int)image.PostID;
                if (System.IO.File.Exists(Server.MapPath(image.Path))) {
                    System.IO.File.Delete(Server.MapPath(image.Path));
                }
                else {
                    throw new System.Exception(Server.MapPath(image.Path));
                }
                repository.Delete(image);
                return RedirectToAction("Details", "Post", new { ID = postID });
            }
            else {
                return HttpNotFound();
            }
        }
    }
}