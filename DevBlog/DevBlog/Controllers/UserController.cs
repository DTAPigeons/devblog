﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using DevBlog.ViewModels.UserViewModels;
using DevBlog.Helpers;
using DevBlog.DataAccess;
using DevBlog.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity;

namespace DevBlog.Controllers
{
    [Authorize(Roles = "Dev")]
    public class UserController : Controller
    {

        UserRepository repository = new UserRepository();
        ApplicationDbContext context = new ApplicationDbContext();

        // GET: User
        public ActionResult Index(UserListingViewModel model,int page= 1, bool descending = true, string sortParameter = "User Name")
        {
            if (page > 1) { model = ControlerHelper.SetUpLisingViewModel<UserListedViewModel>(new UserListingViewModel(page), repository.GetCount()) as UserListingViewModel; }
            else { model = ControlerHelper.SetUpLisingViewModel<UserListedViewModel>(model, repository.GetCount()) as UserListingViewModel; }

            foreach(UserModel user in repository.GetAll(page, model.Pager.PageSize, descending, sortParameter)) {
                UserListedViewModel item = new UserListedViewModel();
                item.SetDataFromModel(user);
                model.Items.Add(item);
            }

            return View(model);
        }
        
        public ActionResult ChangeAutority(int id, string newRole) {

            var UserManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(context));

            UserModel user = repository.GetByID(id);
            if (user == null) {
                return HttpNotFound();
            }
            var userIdentity = UserManager.FindByName(user.UserName);
            if (userIdentity == null) {
                return HttpNotFound();
            }

            string currentRole = user.Role;

            UserManager.AddToRole(userIdentity.Id, newRole);
            UserManager.RemoveFromRole(userIdentity.Id, currentRole);
            user.Role = newRole;
            repository.Save(user);

            return RedirectToAction("Index");
        }
    }
}