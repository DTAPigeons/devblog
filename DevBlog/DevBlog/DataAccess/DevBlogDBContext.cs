﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using DevBlog.Models;
using System.Data.Entity.ModelConfiguration.Conventions;

namespace DevBlog.DataAccess {
    public class DevBlogDBContext: DbContext {
        protected override void OnModelCreating(DbModelBuilder modelBuilder) {
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
        }

        public DevBlogDBContext() : base(){

        }

        DbSet<UserModel> Users { get; set; }
        DbSet<CommentModel> Comments { get; set; }
        DbSet<PostModel> Posts { get; set; }
    }
}