﻿using DevBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace DevBlog.DataAccess {
    public class PostRepository:DevBlogRepository<PostModel> {
        public PostRepository() {

        }

        public override List<PostModel> GetAll(int pageNumber, int pageSize, bool descending, string sortParameter="") {
            List<PostModel> posts = new List<PostModel>();
            switch (sortParameter) {
                case "Author":
                    return base.GetAll<int>(pageNumber, pageSize, AuthorKey(), a => true, descending);
                case "Date":
                    return base.GetAll<DateTime>(pageNumber, pageSize, DateKey(), a => true, descending);
                case "Title":
                    return base.GetAll<string>(pageNumber, pageSize, TitleKey(), a => true, descending);
                default:
                    return base.GetAll<int>(pageNumber, pageSize, ID(), a => true, descending);
            }
        }

        public List<PostModel> GetPostsMadeByAuthor(int pageNumber,int pageSize,int authotID) {
            return base.GetAll<int>(pageNumber, pageSize, ID(), HasAuthor(authotID));
        }

        public Expression<Func<PostModel, bool>> HasAuthor(int authorID) {
            return obj => obj.AuthorID == authorID;
        }

        public Expression<Func<PostModel, int>> ID() {
            return obj => obj.ID;
        }

        public Expression<Func<PostModel, int>> AuthorKey() {
            return obj => obj.AuthorID;
        }

        public Expression<Func<PostModel, DateTime>> DateKey() {
            return obj => obj.TimePosted;
        }

        public Expression<Func<PostModel, string>> TitleKey() {
            return obj => obj.Title;
        }
    }
}