﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;
using System.Data.Entity;
using System.Linq.Expressions;

namespace DevBlog.DataAccess {
    public abstract class DevBlogRepository<T> where T:BaseModel {
        protected DevBlogDBContext context;
        protected DbSet<T> entities;

        public DevBlogRepository() {
            context = new DevBlogDBContext();
            entities = context.Set<T>();
        }
        
        public int GetFirstID() {
            return entities.First().ID;
        }

        public T GetByID(int id) {
            return entities.Where(enity => enity.ID == id).FirstOrDefault();
        }

        public List<T> GetAll() {
            return entities.ToList();
        }

        public List<T> GetAll(int pageNumber=1, int pageSize=100) {
            return entities.OrderBy(entity => entity.ID).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public abstract List<T> GetAll(int pageNumber, int pageSize, bool descending, string sortParameter = "");

        public List<T> GetAll<TKey>(int pageNumber, int pageSize, Expression<Func<T, TKey>> orderFilter, Expression<Func<T, bool>> includeFilter, bool descending = false) {
            if (descending) { return entities.OrderByDescending(orderFilter).Where(includeFilter).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList(); }
            return entities.OrderBy(orderFilter).Where(includeFilter).Skip((pageNumber - 1) * pageSize).Take(pageSize).ToList();
        }

        public int GetCount() {
            return entities.Count();
        }

        public void Save(T entity) {
            if (entity.ID > 0) {
                Update(entity);
            }
            else {
                entity.ID = entities.Count()+1;
                Insert(entity);
            }

            context.SaveChanges();
        }

        private void Insert(T entity) {
            entities.Add(entity);
        }

        private void Update(T entity) {
            context.Entry(entity).State=EntityState.Modified;
        }

        public void Delete(T entity) {
            entities.Remove(entity);
            context.SaveChanges();
        }
    }
}