﻿using DevBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Web;

namespace DevBlog.DataAccess {
    public class UserRepository : DevBlogRepository<UserModel> {
        public override List<UserModel> GetAll(int pageNumber, int pageSize, bool descending, string sortParameter = "") {
            switch (sortParameter) {
                case "User Name":
                    return base.GetAll<string>(pageNumber, pageSize, UserNameKey(), a => true, descending);
                case "Role":
                    return base.GetAll<string>(pageNumber, pageSize, RoleKey(), a => true, descending);
                default:
                    return base.GetAll<int>(pageNumber, pageSize, ID(), a => true, descending);
            }
        }

        public bool UserNameExists(string userName) {
            return entities.FirstOrDefault(user => user.UserName == userName) != null;
        }

        public int GetIDByUserName(string userName) {
           return entities.First(e => e.UserName == userName).ID;
        }

        public Expression<Func<UserModel, int>> ID() {
            return obj => obj.ID;
        }

        public Expression<Func<UserModel, string>> UserNameKey() {
            return obj => obj.UserName;
        }

        public Expression<Func<UserModel, string>> RoleKey() {
            return obj => obj.Role;
        }
    }
}