﻿using DevBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DevBlog.ViewModels.Shared {
    public class ListingViewModel<T> where T : DisplayViewModel {
        public ListingViewModel(){}

        public ListingViewModel(int page) {
            Pager = new Pager();
            Pager.Page = page;
        }

        public Pager Pager { get; set; }

        public List<T> Items { get; set; }
    }
}