﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;

namespace DevBlog.ViewModels.Shared {
    public abstract class DisplayViewModel {
         public int ID { get; set; }
         public abstract void SetDataFromModel(BaseModel model);
    }
}