﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DevBlog.ViewModels.Shared {
    public class Pager {
        public Pager() { }

        public Pager(int pageNumber) {
            Page = pageNumber;
        }

        public const int MAX_PAGE_SIZE = 1;

        public int Page { get; set; }
        public int PageSize { get; set; }
        public int PagesCount { get; set; }
    }
}