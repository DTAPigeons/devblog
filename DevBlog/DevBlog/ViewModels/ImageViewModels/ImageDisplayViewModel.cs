﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.ImageViewModels {
    public class ImageDisplayViewModel : DisplayViewModel {
        public string Path { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            ID = model.ID;
            ImageModel image = model as ImageModel;
            Path = image.Path;
        }
    }
}