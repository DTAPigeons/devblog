﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DevBlog.Models;

namespace DevBlog.ViewModels.ImageViewModels {
    public class ImageUploadViewModel {
        public int PostID { get; set; }
        [DataType(DataType.Upload)]
        [Display(Name = "Upload Image")]
        public string Path { get; set; }

        [Required(ErrorMessage = "Please choose an image to upload.")]
        public HttpPostedFileBase ImageFile { get; set; }

        public ImageModel CreatModel() {
            return new ImageModel() {
                PostID = this.PostID,
                Path = this.Path,
            };
        }
    }
}