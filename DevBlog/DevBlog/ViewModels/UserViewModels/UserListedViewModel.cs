﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.UserViewModels {
    public class UserListedViewModel : DisplayViewModel {
        public string UserName { get; set; }
        public string Role { get; set; }
        public bool Developer { get; set; }
        public bool Banned { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            UserModel user = model as UserModel;
            ID = model.ID;
            UserName = user.UserName;
            Role = user.Role;
            Developer = Role == "Dev";
            Banned = Role == "Banned";
        }
    }
}