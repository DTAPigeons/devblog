﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.UserViewModels {
    public class UserListingViewModel : ListingViewModel<UserListedViewModel> {
        public UserListingViewModel(int page) : base(page) {
            Pager.Page = page;
        }

        public UserListingViewModel() {
        }
    }
}