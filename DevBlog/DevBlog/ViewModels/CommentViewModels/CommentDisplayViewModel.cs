﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;
using DevBlog.ViewModels.Shared;
using System.ComponentModel.DataAnnotations;

namespace DevBlog.ViewModels.CommentViewModels {
    public class CommentDisplayViewModel : DisplayViewModel {
        public string Author { get; set; }
        [MaxLength]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public DateTime TimePosted { get; set; }
        public int? ResponseToID { get; set; }
        public List<CommentDisplayViewModel> Responses { get; set; }
        public int PostID { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            CommentModel comment = model as CommentModel;
            ID = comment.ID;
            Author = comment.Author.UserName;
            Content = comment.Content;
            TimePosted = comment.TimePosted;
            ResponseToID = comment.ResponseToID;
            PostID = comment.PostID;
            if(comment.Responses.Count>0 && comment.Responses != null) {
                CopyResponses(comment.Responses);
            }
        }

        private void CopyResponses(List<CommentModel> responses) {
            Responses = new List<CommentDisplayViewModel>();
            foreach(CommentModel respons in responses) {
                CommentDisplayViewModel model = new CommentDisplayViewModel();
                model.SetDataFromModel(respons);
                Responses.Add(model);
            }
        }
    }
}