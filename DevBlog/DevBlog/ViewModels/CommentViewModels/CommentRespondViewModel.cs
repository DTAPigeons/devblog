﻿using DevBlog.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DevBlog.ViewModels.Shared;
using System.Web.Mvc;

namespace DevBlog.ViewModels.CommentViewModels {
    public class CommentRespondViewModel: DisplayViewModel{
        public CommentDisplayViewModel ResposeTo { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int PostID { get; set; }
        [StringLength(int.MaxValue, MinimumLength = 3)]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        [HiddenInput(DisplayValue = false)]
        public int? ResponseToID { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            CommentModel comment = model as CommentModel;
            ID = comment.ID;
            PostID = comment.PostID;
            Content = comment.Content;
            ResponseToID = comment.ResponseToID;
        }

        public CommentModel CreatModel() {
            return new CommentModel() {
                PostID = this.PostID,
                AuthorID = 1,
                Content = this.Content,
                TimePosted = DateTime.Now,
                ResponseToID = this.ResponseToID
            };
        }
    }
}