﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.PostViewModels {
    public class PostListingViewModel: ListingViewModel<PostListedViewModel>{

        public PostListingViewModel() { }

        public PostListingViewModel(int page) : base(page) { }

    }
}