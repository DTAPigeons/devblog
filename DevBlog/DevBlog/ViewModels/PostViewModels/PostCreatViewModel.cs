﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DevBlog.Models;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.PostViewModels {
    public class PostCreatViewModel: DisplayViewModel{
      
        [StringLength(256)]
        [Required]
        public string Title { get; set; }
        [StringLength(int.MaxValue, MinimumLength = 3)]
        [DataType(DataType.MultilineText)]
        [Required]
        public string Content { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            PostModel post = model as PostModel;
            ID = post.ID;
            Title = post.Title;
            Content = post.Content;
        }

        public PostModel CreateModel() {
            return new PostModel() {
                Title = this.Title,
                Content = this.Content,
                TimePosted = DateTime.Now,
            };
        }
    }
}