﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using DevBlog.Models;
using DevBlog.ViewModels.Shared;

namespace DevBlog.ViewModels.PostViewModels {
    public class PostListedViewModel: DisplayViewModel {
        public string Title { get; set; }
        public string Author { get; set; }
        public DateTime TimePosted { get; set; }

        public override void SetDataFromModel(BaseModel model) {
            PostModel post = model as PostModel;
            ID = post.ID;
            Title = post.Title;
            Author = post.Author.UserName;
            TimePosted = post.TimePosted;
        }
    }
}