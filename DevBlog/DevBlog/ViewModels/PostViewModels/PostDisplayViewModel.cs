﻿using DevBlog.Models;
using DevBlog.ViewModels.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using DevBlog.ViewModels.CommentViewModels;
using System.Linq;
using System.Web;
using DevBlog.ViewModels.ImageViewModels;

namespace DevBlog.ViewModels.PostViewModels {
    public class PostDisplayViewModel : DisplayViewModel {

        public PostDisplayViewModel() {
            ID = 1;
        }
              
        [DisplayName("Author")]
        public string AuthorName { get; set; }
        [DisplayName("Posted on")]
        public DateTime TimePosted { get; set; }
        [StringLength(256)]
        public string Title { get; set; }
        [MaxLength]
        [DataType(DataType.MultilineText)]
        public string Content { get; set; }
        public CommentViewModels.CommentCreateViewModel CommmentCreatModel { get; set; }
        public List<CommentDisplayViewModel> Comments { get; set; }
        public List<ImageDisplayViewModel> Images { get; set; }


        public override void SetDataFromModel(BaseModel model) {
            PostModel post = model as PostModel;
            AuthorName = post.Author.UserName;
            TimePosted = post.TimePosted;
            Title = post.Title;
            Content = post.Content;
            CommmentCreatModel = new CommentCreateViewModel() { PostID = ID };
            CopyComments(post.Comments);
            CopyImages(post.Images);
        }

        private void CopyComments(List<CommentModel> toCopy) {
            Comments = new List<CommentDisplayViewModel>();
            foreach(CommentModel comment in toCopy) {
                CommentDisplayViewModel commentDisplay = new CommentDisplayViewModel();
                commentDisplay.SetDataFromModel(comment);
                Comments.Add(commentDisplay);
            }
        }

        private void CopyImages(List<ImageModel> toCopy) {
            Images = new List<ImageDisplayViewModel>();
            foreach(ImageModel image in toCopy) {
                ImageDisplayViewModel imageDisplay = new ImageDisplayViewModel();
                imageDisplay.SetDataFromModel(image);
                Images.Add(imageDisplay);
            }
        }
    }
}